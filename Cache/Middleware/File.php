<?php

namespace Enova\Cache\Middleware;

use Symfony\Component\Cache\Simple\AbstractCache;
use Slim\Http\Request;
use Slim\Http\Response;

class File {

    private $cache;

    public function __construct(AbstractCache $cache) {
        $this->cache = $cache;
    }

    public function __invoke(Request $request, Response $response, callable $next) {
        $response = $next($request, $response);
        return $response;
    }

}
