<?php

namespace Enova\Cache\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Enova\Core\Utils\Registry;

class CacheClear extends Command {

    protected function configure() {
        $this
                ->setName("cache:clear");
    }

    public function execute(InputInterface $input, OutputInterface $output) {
        $modulesPath = Registry::get('root.directory');
        try {
            $storage = $modulesPath . "/storage";
            shell_exec("ls -ad $storage/cache/* | grep -v '.gitignore' | xargs rm -rf");
            $output->writeln("<info>Cache cleared!</info>");
        } catch (\Exception $ex) {
            $output->writeln($ex->getMessage());
        }
    }

}
